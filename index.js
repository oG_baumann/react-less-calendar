var calendar = function calendar (date) {
	this.active 		= date || this.active;
	this.container		= document.getElementById('calendar');
	this.monthOutput 	= document.getElementById('calendar-month');
	this.yearOutput 	= document.getElementById('calendar-year');
	this.template 		= document.getElementById('calendar-template');
	this.init();
}

calendar.fn = calendar.prototype = {
	active   : new Date(),
	language : 'en-US',
	pages 	 : {},
	monthArr : ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
}

calendar.fn.init = function () {
	var navs = this.container.querySelectorAll('[data-nav]');
	var i = navs.length;
	while(i--){
		navs[i].addEventListener('click', this.navigate.bind(this));
	}
	this.buildPage();
}

calendar.fn.buildPage = function () {
	var d, m, tmp;
	if(!this.pages[this.active.getFullYear()]) {
		this.pages[this.active.getFullYear()] = {};
	}
	if(!this.pages[this.active.getFullYear()][this.active.getMonth()]) {
		this.pages[this.active.getFullYear()][this.active.getMonth()] = {};
	}
	if(!this.pages[this.active.getFullYear()][this.active.getMonth()][this.active.getDay()]){
		d = 1;
		m = this.active.getMonth();
		tmp = new Date(this.active.getFullYear(), this.active.getMonth(), d);
		while(tmp.getMonth() === m) {
			this.pages[this.active.getFullYear()][this.active.getMonth()][d] = {
				data	: {},
				date    : new Date(this.active.getFullYear(), this.active.getMonth(), d)
			};
			d++;
			tmp = new Date(this.active.getFullYear(), this.active.getMonth(), d);
		}
	}
	this.renderPage();
}

calendar.fn.renderPage = function (date) {
	var day, days, parts, row, units, i;
	if(date) {
		this.active = date;
		this.buildPage();
	} else {
		parts = [];
		days = this.pages[this.active.getFullYear()][this.active.getMonth()];
		for(day in days) {
			if(days.hasOwnProperty(day)){
				if(day === '1') {
					var diff = Math.abs(0 - days[day].date.getDay());
					while(diff--) {
						parts.push('<div class="calendar-unit"><div class="calendar-blank"></div></div>');
					}
				}
				parts.push('<div class="calendar-unit"><div><div class="calendar-day">'+day+'</div></div></div>');
			}
		}
		row = "";
		while(parts.length){
			row += '<div class="calendar-row">'+parts.splice(0,7).join('')+'</div>';
		}
		this.monthOutput.innerHTML = this.active.toLocaleString(this.language, {month: 'long'}); 
		this.yearOutput.innerHTML = this.active.getFullYear();
		this.template.innerHTML = row;
		units = this.template.querySelectorAll('.calendar-unit');
		i = units.length;
		while(i--){
			units[i].addEventListener('click', this.getClick.bind(this));
		}
	}
}

calendar.fn.getClick = function (e) {
	var loc = e.currentTarget;
	console.log(loc)
} 

calendar.fn.navigate = function (e) {
	var year = parseInt(this.yearOutput.innerHTML);
	var month = this.monthArr.indexOf(this.monthOutput.innerHTML);
	var nav = e.currentTarget;
	var dir = nav.getAttribute('data-nav');
	if(nav.parentNode.classList.contains('calendar-year')){
		if(dir === "left") {
			this.renderPage(new Date(year - 1, month, 1));
		} else if (dir === "right") {
			this.renderPage(new Date(year + 1, month, 1));
		}
	} else if (nav.parentNode.classList.contains('calendar-month')) {
		if(dir === "left") {
			if(month === 0) {
				month = this.monthArr.length - 1;
				year = year - 1;
			} else {
				month = month - 1;
			}
			this.renderPage(new Date(year, month, 1));
		} else if (dir === "right") {
			if(month === this.monthArr - 1) {
				month = 0;
				year = year + 1;
			} else {
				month = month + 1;
			}
			this.renderPage(new Date(year, month, 1));
		}
	}
}

window.addEventListener('load', function loader () {
	d = new calendar();
});





